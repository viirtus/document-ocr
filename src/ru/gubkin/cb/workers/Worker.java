package ru.gubkin.cb.workers;

import ru.gubkin.cb.cv.*;
import ru.gubkin.cb.document.conf.DocumentConfiguration;
import ru.gubkin.cb.workers.task.Task;
import ru.gubkin.cb.workers.task.TaskListener;
import ru.gubkin.cb.workers.task.provider.TaskProvider;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.concurrent.*;

/**
 * Created by 119-1 on 09.11.2015.
 * Main worker that scan input dir,
 * create and prepare configurations and kitchen,
 * submit tasks into executor or fjp
 * and listen to a tasks when they are completed.
 */
public class Worker implements TaskListener {
    private final DocumentConfiguration docConf;
    private File inputDir;
    //total submitted tasks
    private volatile int totalTasks;
    //total task that has been executed successfully
    private volatile int successfulTasks;
    //total task that has been failed
    private volatile int failedTasks;
//    ForkJoinPool fjp;
    private ExecutorService executor = Executors.newFixedThreadPool(4);

    public Worker(String inputDir, String docConf) throws IOException {
        this.docConf = new DocumentConfiguration(docConf);
        this.inputDir = new File(inputDir);
        if (!this.inputDir.exists()) throw new IllegalArgumentException(inputDir + " is not exist");
//        fjp = new ForkJoinPool();

    }

    /**
     * Run worker to perform computation
     * @throws IOException
     * @throws InterruptedException
     */
    public void run() throws IOException, InterruptedException {
        File[] dirList = inputDir.listFiles();
        if (dirList == null) return;

        ImageKitchen kitchen = new ImageKitchen();
        kitchen.fillFromConfig(docConf);

        int tasks = 0;
        for (File f : dirList) {
            //each file will be processed
            TaskProvider provider = TaskProvider.get(f, docConf, kitchen);
            for (Task t : provider) {
                t.setStateListener(this);
                executor.submit(t::execute);
                totalTasks++;
                tasks++;
            }
            provider.close();

            //KOSTYL (actually, JNI (OpenCV) eat TOO many memory and we wait a time for gc)
            if (tasks > 10) {
                System.gc();
                Thread.sleep(5 * 1000);
                tasks = 0;
            }
        }
        executor.shutdown();
        executor.awaitTermination(50, TimeUnit.SECONDS);
    }

    public String formatFinalLogInfo() {
        String message = String.format(Locale.ENGLISH, "Task run on %s %s", inputDir.getAbsolutePath(), System.lineSeparator());
        message += String.format(Locale.ENGLISH, "Total tasks executed: %d; Successful tasks: %d; Failed: %d %s", totalTasks, successfulTasks, failedTasks, System.lineSeparator());
        return message;
    }

    /**
     * Increments counters.
     * @param state is task successful or not
     */
    @Override
    public void onTaskComplete(boolean state) {
        if (state) successfulTasks++;
        else failedTasks++;
    }
}
