package ru.gubkin.cb.workers.task;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import ru.gubkin.cb.document.DefaultDocument;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by 119 on 19.01.2016.
 * As the RenameTask, PdfRenameTask create a new file for document
 * using name mask from config. File will be a valid pdf file.
 */
public class PdfRenameTask extends RenameTask {
    private static final float MAGIC_WIDTH = 595.27563F;
    private static final float MAGIC_HEIGHT = 841.8898F;

    public PdfRenameTask(DefaultDocument document, String successFolder, String errorFolder) {
        super(document, successFolder, errorFolder);
    }

    /**
     * Create a new pdf file in state match directory.
     * Pdf contain SOURCE image from document.
     * TODO SWITCH SOURCE AND TRANSFORMED IMAGES IN CONFIG
     */
    @Override
    public void execute() {
        String fileName = recognize();
        File dir = isTaskSuccess ? successFolder : errorFolder;
        if (!isTaskSuccess) fileName = document.getName();

        File target = createTargetFile(fileName, dir);
        PDDocument pdDocument = new PDDocument();
        PDPage page = new PDPage(PDPage.PAGE_SIZE_A4);
        try {
            BufferedImage image = document.raw();
            float width = MAGIC_WIDTH;
            float height = MAGIC_HEIGHT;
            int imageWidth = image.getWidth();
            int imageHeight = image.getHeight();

            //fill by width
            if (imageWidth > imageHeight) {
                height = MAGIC_WIDTH * imageHeight / imageWidth;
            } else {
                width = MAGIC_HEIGHT * imageWidth / imageHeight;
            }

            pdDocument.addPage(page);
            PDJpeg img = new PDJpeg(pdDocument, image);
            PDPageContentStream pcs = new PDPageContentStream(pdDocument, page);
            pcs.drawXObject(img, 0, 0, width, height);
            pcs.close();
            pdDocument.save(target);
            pdDocument.close();
        } catch (IOException | COSVisitorException e) {
            isTaskSuccess = false;
            e.printStackTrace();
        }
        notifyComplete();
    }
}
