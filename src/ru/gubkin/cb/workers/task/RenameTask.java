package ru.gubkin.cb.workers.task;

import ru.gubkin.cb.document.DefaultDocument;
import ru.gubkin.cb.util.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by 119 on 19.01.2016.
 * Rename task is a class of a task that instead of simply text recognition
 * can also rename file (in general, create new) in conformity with document mask from config
 * and place it into success of fail folder in depend of document recognition state.
 */
public abstract class RenameTask extends Task {
    protected final File successFolder;
    protected final File errorFolder;

    /**
     * @param document for perform
     * @param successFolder path to folder for store successful documents
     * @param errorFolder path to folder for store failed documents
     */
    public RenameTask(DefaultDocument document, String successFolder, String errorFolder) {
        super(document);
        this.successFolder = new File(successFolder);
        this.errorFolder = new File(errorFolder);
        mkSubDirs();
    }

    protected void mkSubDirs() {
        //TODO HANDLE IT
        if (!successFolder.exists()) {
            boolean successFolderState = successFolder.mkdir();
        }
        if (!errorFolder.exists()) {
            boolean errorFolderState = errorFolder.mkdir();
        }
    }

    /**
     * Method return a File for given name.
     * If file with given name already exist, it return new sequence-name file.
     * Example: "test.txt" is already exist, then return "test (1).txt"
     * @param needed given file name
     * @param superDir target dir
     * @return new file with given name
     */
    protected File createTargetFile(String needed, File superDir) {
        File target = new File(superDir.getAbsoluteFile() + File.separator + needed);
        if (!target.exists()) return target;

        String fileName = Util.clearFileName(needed);
        Pattern pattern = Pattern.compile(needed + " \\((\\d+)\\)\\..+");
        File[] dirList = superDir.listFiles();

        // dirList will never be null
        ArrayList<File> files = new ArrayList<>(Arrays.asList(dirList));
        int max =  files.parallelStream()
                .mapToInt((f) -> {
                    String name = f.getName();
                    Matcher matcher = pattern.matcher(name);
                    if (!matcher.matches()) return 0;
                    return Integer.parseInt(matcher.group(1));
                }).max().getAsInt();

        fileName += " (" + (max + 1) + ")" + Util.fileExt(needed);
        return new File(superDir.getAbsoluteFile() + File.separator + fileName);
    }


}
