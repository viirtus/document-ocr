package ru.gubkin.cb.workers.task.provider;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import ru.gubkin.cb.cv.ImageKitchen;
import ru.gubkin.cb.document.DefaultDocument;
import ru.gubkin.cb.document.DocumentFactory;
import ru.gubkin.cb.document.conf.DocumentConfiguration;
import ru.gubkin.cb.util.Util;
import ru.gubkin.cb.workers.task.TaskFactory;
import ru.gubkin.cb.workers.task.Task;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Created by 119 on 19.01.2016.
 * Provide a tasks for pdf documents.
 * Each page will be wrapped as a task.
 */
public class PdfTaskProvider extends TaskProvider {
    private PDDocument document;
    private Iterator<PDPage> innerIterator;
    private int page = 0;

    /**
     * Constructor make a PDDocument from given file and get a iterator for pages from it.
     * @param src @see super(...)
     * @param configuration @see super(...)
     * @param kitchen @see super(...)
     */
    protected PdfTaskProvider(File src, DocumentConfiguration configuration, ImageKitchen kitchen) {
        super(src, configuration, kitchen);
        try {
            document = PDDocument.load(src);
            List<PDPage> pages = document.getDocumentCatalog().getAllPages();
            innerIterator = pages.iterator();
        } catch (IOException e) {
            //broken document or input fail
            //provider will be empty
            document = null;
        }
    }

    @Override
    public boolean hasNext() {
        return innerIterator != null && innerIterator.hasNext();
    }

    /**
     * @return next task for next page.
     */
    @Override
    public Task next() {
        try {
            DefaultDocument d =  DocumentFactory.createPdfDocument(innerIterator.next(), configuration);
            d.setName(Util.clearFileName(src) + " (page_" + (page++) + ").pdf");
            d.applyTransform(kitchen);
            return TaskFactory.createTaskForConfig(d, configuration);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public void close() {
        try {
            document.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isAllow(File file) {
        return Util.fileExt(file).equals(".pdf");
    }
}
