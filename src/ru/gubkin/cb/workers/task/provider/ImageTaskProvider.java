package ru.gubkin.cb.workers.task.provider;

import ru.gubkin.cb.cv.ImageKitchen;
import ru.gubkin.cb.document.DefaultDocument;
import ru.gubkin.cb.document.DocumentFactory;
import ru.gubkin.cb.document.conf.DocumentConfiguration;
import ru.gubkin.cb.util.Util;
import ru.gubkin.cb.workers.task.TaskFactory;
import ru.gubkin.cb.workers.task.Task;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by 119 on 21.01.2016.
 * Provider for simple images.
 */
public class ImageTaskProvider extends TaskProvider {
    //flag for hasNext method
    private boolean hasImage;
    //allow file extensions that can be read by this provider
    private static final List<String> allowExt = Arrays.asList(".png", ".jpg", ".jpeg", ".bmp");

    protected ImageTaskProvider(File src, DocumentConfiguration configuration, ImageKitchen kitchen) {
        super(src, configuration, kitchen);
        hasImage = true;
    }

    @Override
    public boolean hasNext() {
        return hasImage;
    }

    @Override
    public Task next() {
        try {
            //read image and get a document for it
            BufferedImage image = ImageIO.read(src);
            DefaultDocument d =  DocumentFactory.createDocument(image, configuration);
            d.setName(src.getName());
            d.applyTransform(kitchen);

            //no more images
            hasImage = false;
            return TaskFactory.createTaskForConfig(d, configuration);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return super.next();
    }

    protected static boolean isAllow(File file) {
        return allowExt.contains(Util.fileExt(file));
    }
}
