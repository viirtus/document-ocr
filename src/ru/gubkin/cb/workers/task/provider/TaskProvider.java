package ru.gubkin.cb.workers.task.provider;

import ru.gubkin.cb.cv.ImageKitchen;
import ru.gubkin.cb.document.conf.DocumentConfiguration;
import ru.gubkin.cb.util.Util;
import ru.gubkin.cb.workers.task.Task;

import java.io.File;
import java.util.Iterator;

/**
 * Created by 119 on 19.01.2016.
 * Task provider is used for getting a next task.
 * Task can be provided by iterator.
 * Task is a implementation a Task class.
 * Provider is used for hiding communication with file content.
 */
public class TaskProvider implements Iterable<Task>, Iterator<Task> {
    protected File src;
    protected DocumentConfiguration configuration;
    protected ImageKitchen kitchen;

    /**
     * Provider cannot be created from outside packages.
     * It can be created only via "get" static factory method.
     * @param src source file
     * @param configuration document configuration
     * @param kitchen image transform that has been applied to each task.
     */
    protected TaskProvider (File src, DocumentConfiguration configuration, ImageKitchen kitchen) {
        this.src = src;
        this.configuration = configuration;
        this.kitchen = kitchen;
    }

    /**
     * Empty provider can be get only via "get" method if there is no matched providers.
     */
    private TaskProvider () {}

    /**
     * @return iterator for tasks
     */
    @Override
    public Iterator<Task> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public Task next() {
        return null;
    }


    /**
     * A factory method for create a new instance for given file.
     * @param f source file for perform
     * @param configuration document type configuration
     * @param kitchen image transform that has been applied to each task.
     * @return new instance that matched given file.
     */
    public static TaskProvider get(File f, DocumentConfiguration configuration, ImageKitchen kitchen) {
        if (PdfTaskProvider.isAllow(f)) return new PdfTaskProvider(f, configuration, kitchen);
        if (ImageTaskProvider.isAllow(f)) return new ImageTaskProvider(f, configuration, kitchen);

        //empty provider if we have no providers for file type
        return new TaskProvider();
    }

    /**
     * If file (or resource) must be closed after work.
     */
    public void close() {}
}
