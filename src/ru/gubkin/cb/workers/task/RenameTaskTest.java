package ru.gubkin.cb.workers.task;

import org.apache.pdfbox.pdmodel.PDPage;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.gubkin.cb.document.PdfDocument;

import java.io.File;

/**
 * Created by 119 on 19.01.2016.
 */
public class RenameTaskTest {

    @Test
    public void testCreateTargetFile() throws Exception {
        PdfRenameTask renameTask = new PdfRenameTask(new PdfDocument(new PDPage()), "s", "e");
        String file1 = "98567.pdf";
        File file_1 = renameTask.createTargetFile(file1, new File("s"));
        Assert.assertEquals(file1, file_1.getName());

        String file2 = "98566.pdf";
        File file_2 = renameTask.createTargetFile(file2, new File("s"));
        Assert.assertEquals("98566 (2).pdf", file_2.getName());

    }
}