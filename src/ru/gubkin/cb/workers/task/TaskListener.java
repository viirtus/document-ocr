package ru.gubkin.cb.workers.task;

/**
 * Created by 119 on 20.01.2016.
 * Task listener provide an interface for communication
 * between listener and task.
 */
public interface TaskListener {
    /**
     * Fired when task is completed
     * @param state is task successful or not
     */
    void onTaskComplete(boolean state);
}
