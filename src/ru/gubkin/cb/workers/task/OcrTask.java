package ru.gubkin.cb.workers.task;

import ru.gubkin.cb.document.DefaultDocument;
import ru.gubkin.cb.util.Util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by 119 on 21.01.2016.
 * Ocr task is a task that simply recognize document field-by-field
 * and save it in conformity with document mask from config by the same name.
 */
public class OcrTask extends Task {
    private final File superDir;

    /**
     * @param document for recognition
     * @param superDir where is text file will be located
     */
    public OcrTask(DefaultDocument document, File superDir) {
        super(document);
        this.superDir = superDir;
    }

    public OcrTask(DefaultDocument document) {
        //-------------work dir
        this(document, new File(""));
    }

    /**
     * Simply write the recognized text into text file.
     */
    @Override
    public void execute() {
        String rec = recognize();
        File target = new File(superDir.getAbsolutePath() + File.separator + Util.clearFileName(document.getName()) + ".txt");
        try {
            FileWriter writer = new FileWriter(target);
            writer.write(rec);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        notifyComplete();
    }
}
