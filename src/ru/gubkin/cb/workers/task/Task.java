package ru.gubkin.cb.workers.task;

import ru.gubkin.cb.document.DefaultDocument;
import ru.gubkin.cb.ocr.DocumentOCR;
import ru.gubkin.cb.util.Util;

/**
 * Created by 119 on 19.01.2016.
 * Abstract task.
 * Task is a document wrapper that perform all work.
 * Work is an OCR process by DocumentOCR.
 * Task can be different kinds and specified in a config file.
 *
 */
public abstract class Task {
    protected volatile boolean isTaskSuccess;
    protected final DefaultDocument document;
    private final DocumentOCR documentOCR;
    private TaskListener listener;

    /**
     * Create a task from document.
     * @param document need to be ocr
     */
    public Task(DefaultDocument document) {
        this.document = document;
        this.documentOCR = new DocumentOCR(this.document);
    }

    /**
     * Actually, method must be called once, but for safety it's synchronized
     * @return an ocr string
     */
    public synchronized String recognize() {
        isTaskSuccess = documentOCR.recognize();
        String r = documentOCR.getMaskResult();
        log();
        return r;
    }

    private void log() {
        String logFolder = Util.clearFileName(document.getName());
        document.log(logFolder, isTaskSuccess);
    }

    /**
     * Setting listener for task state.
     * @param listener instance
     */
    public void setStateListener(TaskListener listener) {
        this.listener = listener;
    }

    /**
     * Notify listener that all task is complete
     */
    protected void notifyComplete() {
        if (listener != null) listener.onTaskComplete(isTaskSuccess);
    }

    /**
     * Run task execution.
     */
    public abstract void execute();
}
