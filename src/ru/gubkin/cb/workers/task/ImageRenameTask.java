package ru.gubkin.cb.workers.task;

import ru.gubkin.cb.document.DefaultDocument;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * Created by 119 on 21.01.2016.
 * As the RenameTask, ImageRenameTask create a new file for document
 * using name mask from config. File will be a valid image with specified in config format.
 */
public class ImageRenameTask extends RenameTask {
    //image output format
    private String format = "png";

    public ImageRenameTask(DefaultDocument document, String successFolder, String errorFolder) {
        super(document, successFolder, errorFolder);
    }

    public void setImageFormat(String format) {
        this.format = format;
    }

    /**
     * Create a new image with given format.
     * Image will be SOURCE image from document.
     */
    @Override
    public void execute() {
        String fileName = recognize();
        File dir = isTaskSuccess ? successFolder : errorFolder;
        if (!isTaskSuccess) fileName = document.getName();
        File target = createTargetFile(fileName, dir);

        try {
            ImageIO.write(document.raw(), format, target);
        } catch (IOException e) {
            isTaskSuccess = false;
            e.printStackTrace();
        }
    }
}
