package ru.gubkin.cb.workers.task;

import ru.gubkin.cb.document.DefaultDocument;
import ru.gubkin.cb.document.conf.DocumentConfiguration;
import ru.gubkin.cb.workers.task.ImageRenameTask;
import ru.gubkin.cb.workers.task.OcrTask;
import ru.gubkin.cb.workers.task.PdfRenameTask;
import ru.gubkin.cb.workers.task.Task;

import java.io.File;

/**
 * Created by 119 on 19.01.2016.
 * A factory create needed task from document and config.
 *
 */
public class TaskFactory {
    private static final String MODE_RENAME = "rename";
    private static final String MODE_OCR = "ocr";
    private static final String SAVE_FORMAT_PDF = "pdf";
    private static final String SAVE_FORMAT_IMAGE_PNG = "png";
    private static final String SAVE_FORMAT_IMAGE_JPG = "jpg";
    private static final String SAVE_FORMAT_IMAGE_BMP = "bmp";
    private static final String RENAME_SUCCESS_FOLDER = "success";
    private static final String RENAME_FAIL_FOLDER = "fail";

    /**
     * Create new task for given configuration and document
     * @param document need to be processed
     * @param conf task info provider
     * @return new Task
     */
    public static Task createTaskForConfig(DefaultDocument document, DocumentConfiguration conf) {
        Task task = null;
        String mode = conf.getMode();

        if (mode.equals(MODE_RENAME)) {
            String saveMode = conf.getSaveMode();
            String successFolder = conf.getSuccessFolder();
            String failFolder = conf.getFailFolder();
            successFolder = successFolder.isEmpty() ? RENAME_SUCCESS_FOLDER : successFolder;
            failFolder = failFolder.isEmpty() ? RENAME_FAIL_FOLDER : failFolder;
            if (saveMode.equals(SAVE_FORMAT_PDF)) {
                task = new PdfRenameTask(document, successFolder, failFolder);
            }
            if (saveMode.equals(SAVE_FORMAT_IMAGE_PNG) ||
                    saveMode.equals(SAVE_FORMAT_IMAGE_JPG) ||
                    saveMode.equals(SAVE_FORMAT_IMAGE_BMP)) {
                ImageRenameTask _task = new ImageRenameTask(document, successFolder, failFolder);
                _task.setImageFormat(saveMode);
                task = _task;
            }
        }

        if (mode.equals(MODE_OCR)) {
            String successFolder = conf.getSuccessFolder();
            successFolder = successFolder.isEmpty() ? RENAME_SUCCESS_FOLDER : successFolder;
            File folder = new File(successFolder);
            folder.mkdirs();
            task = new OcrTask(document, folder);
        }

        //if no of task matched, than create an empty task
        if (task == null) task = dummyTask();

        return task;
    }

    private static Task dummyTask() {
        return new Task(null){
            @Override
            public void execute() {

            }
        };
    }
}
