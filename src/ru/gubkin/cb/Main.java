package ru.gubkin.cb;

import ru.gubkin.cb.util.Logger;
import ru.gubkin.cb.workers.Worker;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by 119-1 on 06.11.2015.
 */
public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        long start = System.currentTimeMillis();

        //input dir
        String input = "agreement_ocr";
        String conf = input + File.separator + "conf.json";

        /***<MAIN CODE>***/
        Worker worker = new Worker(input, conf);
        worker.run();
        /***</MAIN CODE>***/

        //logs
        long end = System.currentTimeMillis();
        System.out.println("Time spent: " + (end - start) + "ms");
        String finalelog = worker.formatFinalLogInfo();
        finalelog += String.format(Locale.ENGLISH, "Time spent: %d ms", (end - start));
        Logger.log("", "log.txt", finalelog);

    }
}
