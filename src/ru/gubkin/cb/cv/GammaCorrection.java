package ru.gubkin.cb.cv;

import java.awt.image.BufferedImage;

/**
 * Created by Kirill on 07.09.2015.
 */
public class GammaCorrection extends Algo {
    private final int imageChanel;
    private final double gamma;

    public GammaCorrection(int imageChanel, double gamma) {
        this.imageChanel = imageChanel;
        this.gamma = gamma;
    }

    @Override
    public BufferedImage doWith(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        BufferedImage blank = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {
                int pixel = image.getRGB(i, j);
                int[] rgb = getRgb(pixel);
                int chanelPosition = chanelPosition(imageChanel);
                if (chanelPosition != GRAY) {
                    rgb[chanelPosition] = gamma(rgb[chanelPosition], gamma);
                    blank.setRGB(i, j, rgbToColor(rgb[2], rgb[3], rgb[4]));
                } else {
                    int brightness = rgb[2];
                    int corrected = gamma(brightness, gamma);
                    blank.setRGB(i, j, rgbToColor(corrected, corrected, corrected));
                }
            }
        };
        return blank;
    }

    public static int gamma(int chanel, double gamma) {
        return (int) (0xFF * Math.pow((chanel * 1.0 / 0xFF), gamma));
    }
}
