package ru.gubkin.cb.cv;

import org.bytedeco.javacpp.*;
import ru.gubkin.cb.util.Util;
import ru.gubkin.cb.workers.Worker;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;

/**
 * C to Java translation of the houghlines.c sample provided in the c sample directory of OpenCV 2.1,
 * using the JavaCV Java wrapper of OpenCV 2.2 developped by Samuel Audet.
 *
 * @author Jeremy Nicola
 *         jeremy.nicola@gmail.com
 */
public class HoughLines {

    private static CvMemStorage storage;

    /**
     * Must be synchronized!!!
     */
    public synchronized static double defineRotateAngle(BufferedImage image) {

        IplImage src = Util.bufferedImageToIpl(image);
        IplImage dst;
        IplImage colorDst;
        if (storage == null) {
            storage = cvCreateMemStorage(0);
        }
        CvSeq lines = new CvSeq();
        dst = cvCreateImage(cvGetSize(src), src.depth(), 1);

        colorDst = cvCreateImage(cvGetSize(src), src.depth(), 3);
        cvCanny(src, dst, 50, 200, 3);
        cvCvtColor(dst, colorDst, CV_GRAY2BGR);

        lines = cvHoughLines2(dst, storage, CV_HOUGH_PROBABILISTIC, 1, Math.PI / 180, 40, 50, 10, 0, CV_PI);
        ArrayList<Double> tangs = new ArrayList<>();

        for (int i = 0; i < lines.total(); i++) {
            Pointer line = cvGetSeqElem(lines, i);
            CvPoint pt1 = new CvPoint(line).position(0);
            CvPoint pt2 = new CvPoint(line).position(1);
            double dx = pt2.x() - pt1.x();
            double dy = pt2.y() - pt1.y();
            if (dx != 0) {
                tangs.add(dy / dx);
            }

        }

        Collections.sort(tangs);
        int tenPercent = lines.total() / 10;
        int middle = tangs.size() / 2;
        double middleAvg = 0;
        for (int i = middle - tenPercent; i < middle + tenPercent; i++) {
            middleAvg += tangs.get(i);
        }
        double tan = middleAvg / (tenPercent * 2);
        return Math.toDegrees(Math.atan(tan));

//        }
    }
}
