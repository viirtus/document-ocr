package ru.gubkin.cb.cv;

import org.json.JSONObject;
import ru.gubkin.cb.document.conf.DocumentConfiguration;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by 119-1 on 09.11.2015.
 * Class for multi-process image. It seems like a simple conveyor.
 * Order will be saved.
 */
public class ImageKitchen {
    /**
     * Main algorithms chain
     */
    private ArrayList<Algo> chain = new ArrayList<>();

    /**
     * Append an algo to chain.
     * @param a an algorithm for image processing
     */
    public void append(Algo a) {
        chain.add(a);
    }

    /**
     * Run sequentially each algo from chain with image from previous step.
     * @param image raw image for process.
     * @return result of image processing.
     */
    public BufferedImage process(BufferedImage image) {

        for (Algo a : chain) {
            image = a.doWith(image);
        }

        return image;
    }

    /**
     * Filling a chain from config.
     * @param conf which used for filling
     */
    public void fillFromConfig(DocumentConfiguration conf) {
        if (conf.isTransformAvailable()) {
            Object an = conf.getTransform("angleNormalize");
            Object gr = conf.getTransform("gray");
            Object ga = conf.getTransform("gamma");
            Object bi = conf.getTransform("binarize");
            if (gr != null) {
                if ((Boolean) gr) append(new Grayscale());
            }

            if (ga != null) {
                if (ga instanceof Boolean) append(new GammaCorrection(Algo.GRAY, 1));
                if (ga instanceof JSONObject) {
                    JSONObject o = (JSONObject) ga;
                    String chanel = o.getString("chanel");
                    int _c;
                    switch (chanel) {
                        case "red" : _c = Algo.RED_CHANEL; break;
                        case "green" : _c = Algo.GREEN_CHANEL; break;
                        case "blue" : _c = Algo.BLUE_CHANEL; break;
                        default: _c = Algo.GRAY;
                    }
                    double y = o.getDouble("y");
                    append(new GammaCorrection(_c, y));
                }
            }

            if (bi != null) {
                if (bi instanceof Boolean) append(new OtsuBinarize());
                if (bi instanceof JSONObject) {
                    JSONObject o = (JSONObject) bi;
                    int threshold = o.getInt("threshold");
                    append(new OtsuBinarize(threshold));
                }
            }

            if (an != null) {
                if ((Boolean) an) append(new AngleNormalize());
            }

        }
    }

}
