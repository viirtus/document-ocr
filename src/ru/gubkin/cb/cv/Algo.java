package ru.gubkin.cb.cv;

import java.awt.image.BufferedImage;
import java.util.function.Consumer;

/**
 * Created by 119-1 on 09.11.2015.
 * An abstract class for representation of algorithm
 * for image processing.
 */
public abstract class Algo {
    public static final int RED_CHANEL = 16;
    public static final int GREEN_CHANEL = 8;
    public static final int BLUE_CHANEL = 0;
    public static final int GRAY = -1;

    /**
     * Abstract method for process image.
     * @param image need to be processed. Image will not be modified.
     * @return new processed image
     */
    protected abstract BufferedImage doWith(BufferedImage image);

    protected static int[] getRgb(int pixel) {
        int[] rgb = new int[3];
        rgb[0] = (pixel >> RED_CHANEL) & 0xFF;
        rgb[1] = (pixel >> GREEN_CHANEL) & 0xFF;
        rgb[2] = (pixel >> BLUE_CHANEL) & 0xFF;
        return rgb;
    }

    /**
     * Convert rgb 10-int into hex-color
     * @param red
     * @param green
     * @param blue
     * @return hex color for given color
     */
    protected int rgbToColor(int red, int green, int blue){
        int newPixel;
        newPixel = red << RED_CHANEL;
        newPixel += green << GREEN_CHANEL;
        newPixel += blue << BLUE_CHANEL;
        return newPixel;
    }

    /**
     * Map chanel to the position in holder array
     * @param chanel static
     * @return
     */
    protected int chanelPosition(int chanel) {
        switch (chanel) {
            case RED_CHANEL: return 0;
            case GREEN_CHANEL: return 1;
            case BLUE_CHANEL: return 2;
            default: return GRAY;
        }
    }
}
