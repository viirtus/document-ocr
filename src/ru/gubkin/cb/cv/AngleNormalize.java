package ru.gubkin.cb.cv;

import ru.gubkin.cb.util.Util;

import java.awt.image.BufferedImage;

/**
 * Created by 119-1 on 09.11.2015.
 * Class is used for normalize image orientation.
 * Class will rotate image for normal vertical orientation.
 * Class is based on OpenCV library, that run in native mode.
 *
 * If in runtime will be thrown an native exception, it signal,
 * that heap size is not enough.
 */
public class AngleNormalize extends Algo {
    @Override
    protected BufferedImage doWith(BufferedImage image) {
        double angle = HoughLines.defineRotateAngle(image);
        return Util.rotateImage(image, Math.toRadians(-angle));
    }
}
