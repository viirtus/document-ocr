package ru.gubkin.cb.cv;

import java.awt.image.BufferedImage;

/**
 * Created by Kirill on 06.09.2015.
 * Class for converting image into grayscale image;
 */
public class Grayscale extends Algo {
    private static final float HDTV_RED_M = 0.2126f;
    private static final float HDTV_GREEN_M = 0.7152f;
    private static final float HDTV_BLUE_M = 0.0722f;

    @Override
    public BufferedImage doWith(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        BufferedImage blank = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {
                int pixel = image.getRGB(i, j);
                int[] rgb = getRgb(pixel);
                int _y = getBrightness(rgb[0], rgb[1], rgb[2]);
                blank.setRGB(i, j, rgbToColor(_y, _y, _y));
            }
        }
        return blank;
    }


    private static int getBrightness(int red, int green, int blue) {
        int r = Math.round(red * HDTV_RED_M);
        int g = Math.round(green * HDTV_GREEN_M);
        int b = Math.round(blue * HDTV_BLUE_M);
        return r + g + b;
    }
}
