package ru.gubkin.cb.ocr;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import ru.gubkin.cb.document.DefaultDocument;
import ru.gubkin.cb.document.field.Field;

import java.awt.*;
import java.util.Collections;

/**
 * Created by 119 on 14.01.2016.
 * Core of recognition.
 * In heart, we use a Tesserat open source project.
 * In work in native lib too.
 * Class is not thread-safe.
 */
public class DocumentOCR {
    private static final String FAIL = "\0";
    private DefaultDocument document;

    /**
     * Specify a document for recognition.
     * @param document
     */
    public DocumentOCR(DefaultDocument document) {
        this.document = document;
    }

    /**
     * Making ocr using Tesseract for each document field.
     * @return true if all of fields is valid, false otherwise.
     */
    public boolean recognize() {
        Tesseract tesseract = new Tesseract();
        Field[] fields = document.getFields();
        boolean state = true;
        for (Field f : fields) {
            String tessConf = f.getTesseractCharsetDetail();

            if (tessConf != null) {
                //must be valid path, relative to ../tessdata/configs/
                tesseract.setConfigs(Collections.singletonList(tessConf));
            }
            Rectangle rect = f.getArea();
            String value = doOcr(rect, tesseract);
            f.setValue(value);
            f.clean();

            boolean _state = f.check();
            if (!_state) state = false;

            //restore context.
            tesseract.setConfigs(null);
        }

        return state;
    }


    /**
     * @return result of ocr using mask from document.
     */
    public String getMaskResult() {
        String mask = document.getMask();
        Field[] fields = document.getFields();
        for (Field f : fields) {
            mask = mask.replaceAll("\\{" + f.getName() + "\\}", f.getClearValue());
        }
        return mask;
    }

    public String getUnmaskResult() {
        String r = "";
        Field[] fields = document.getFields();
        for (Field f : fields) {
            r += f.getClearValue() + " ";
        }
        return r;
    }

    /**
     * @param rectangle ocr field
     * @param tesseract instance
     * @return ocr string
     */
    private String doOcr(Rectangle rectangle, Tesseract tesseract) {
        try {
            return tesseract.doOCR(document.image(), rectangle);
        } catch (TesseractException e) {
            return FAIL;
        }

    }
}
