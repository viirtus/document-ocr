package ru.gubkin.cb.util;

import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by 119-1 on 09.11.2015.
 */
public class Logger {
    private static String logPath;
    static {
        logPath = "log" + File.separator + nowDay() + File.separator;
        File dir = new File(logPath);
        if (!dir.exists()) {
            dir.mkdir();
        }
    }

    public static void log(String tag, String name, BufferedImage image) {
        File filePath = createLogFile(tag, name);

        try {
            ImageIO.write(image, "png", filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static  void log(String tag, String name, String text) {
        File filePath = createLogFile(tag, name);
        try {
            FileWriter writer = new FileWriter(filePath, true);
            writer.append("[").append(nowTime()).append("]: ");
            writer.append(text).append(System.lineSeparator());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static File createLogFile(String tag, String name) {
        String _logPath = logPath + tag;
        File tagDir = new File(_logPath);
        if (!tagDir.exists()) {
            tagDir.mkdirs();
        }

        return new File(_logPath + File.separator + name);
    }

    @NotNull
    private static String nowDay() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
        return formatter.format(LocalDate.now());
    }

    @NotNull
    private static String nowTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd H:m:s");
        return formatter.format(LocalDateTime.now());
    }
}
