package ru.gubkin.cb.util;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by 119-1 on 09.11.2015.
 */
public class Loader {
    public static BufferedImage loadDirect(File src) throws IOException {
        return ImageIO.read(src);
    }

    public static BufferedImage loadDirect(String src) throws IOException {
        return loadDirect(new File(src));
    }


}
