package ru.gubkin.cb.util;

import org.apache.pdfbox.pdmodel.PDPage;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.OpenCVFrameConverter;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by 119-1 on 09.11.2015.
 */
public class Util {
    public static opencv_core.IplImage bufferedImageToIpl(BufferedImage image) {
        Java2DFrameConverter converter1 = new Java2DFrameConverter();
        OpenCVFrameConverter.ToIplImage converter2 = new OpenCVFrameConverter.ToIplImage();
        return converter2.convert(converter1.convert(image));
    }

    /**
     * Rotate given image for angle in radians
     * @param image need to be rotated
     * @param radians angle in radians
     * @return new rotated BI
     */
    public static BufferedImage rotateImage(BufferedImage image, double radians) {
        AffineTransform transform = new AffineTransform();
        transform.rotate(radians, image.getWidth()/2, image.getHeight()/2);
        AffineTransformOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);
        return op.filter(image, null);
    }

    /**
     * Extract file name without extension
     * @param name full file name
     * @return file name only
     */
    public synchronized static String clearFileName(String name) {
        int dotIndex = name.lastIndexOf(".");
        return name.substring(0, dotIndex);
    }

    /**
     * Extract file ext with leading dot (.)
     * @param fileName full time name
     * @return file ext
     */
    public static String fileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf("."));
    }

    public static String fileExt(File file) {
        return fileExt(file.getName());
    }

    public static String clearFileName(File src) {
        return clearFileName(src.getName());
    }
}
