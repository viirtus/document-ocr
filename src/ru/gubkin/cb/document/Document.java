package ru.gubkin.cb.document;

import com.sun.istack.internal.NotNull;
import ru.gubkin.cb.cv.ImageKitchen;
import ru.gubkin.cb.util.Loader;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by 119-1 on 09.11.2015.
 * Representation simple document.
 * Source - image.
 * Document also has name.
 */
public abstract class Document{
    //before transform.
    BufferedImage source;
    //after transform.
    BufferedImage processed;
    //document name.
    private String name;

    public Document(File src) throws IOException {
        this.source = Loader.loadDirect(src);
    }

    public Document(String src) throws IOException {
        this.source = Loader.loadDirect(src);
    }

    public Document(@NotNull BufferedImage image) {
        this.source = image;
    }

    public void setName(String name) {
        this.name = name;
    }

    //apply kitchen to document.
    //source will not be modified.
    public void applyTransform(ImageKitchen kitchen) {
        processed = kitchen.process(this.source);
    }

    /**
     * @return an source image, that has been used in constructor
     */
    public BufferedImage raw() {
        return source;
    }

    /**
     * @return document image. If there is processed image, it will be returned.
     */
    public BufferedImage image() {
        if (processed == null) return source;
        return processed;
    }

    /**
     * If document need to be closed.
     */
    public void close(){};

    public String getName() {
        return name;
    }
}
