package ru.gubkin.cb.document;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;

import java.io.IOException;

/**
 * Created by 119 on 19.01.2016.
 * In general, add one more constructor to super class.
 * It allow to create document from PDPage
 */
public class PdfDocument extends DefaultDocument {
    private PDPage page;
    public PdfDocument(PDPage src) throws IOException {
        super(src.convertToImage(1, 362));
        this.page = src;
    }

    public PDPage getPage() {
        return page;
    }
}
