package ru.gubkin.cb.document;

import org.apache.pdfbox.pdmodel.PDPage;
import ru.gubkin.cb.document.conf.DocumentConfiguration;
import ru.gubkin.cb.document.field.Field;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created by 119 on 14.01.2016.
 * Used for construct all if document types from source and config.
 */
public class DocumentFactory {
    private static FieldFactory fieldFactory;

    /**
     * Create an PdfDocument.
     * @param page that will be used in document.
     * @param conf document config that will apply to document
     * @return an object DefaultDocument type
     * @throws IOException
     */
    public static DefaultDocument createPdfDocument(PDPage page, DocumentConfiguration conf) throws IOException {

        /**
         * Caching data for better performance
         */
        if (fieldFactory == null) {
            fieldFactory = new FieldFactory(conf);
        }

        DefaultDocument document = new PdfDocument(page);
        int dpi = document.getDpi();
        Field[] fields = fieldFactory.fromConfig(dpi);
        document.setFieldList(fields);
        document.setMask(conf.getFileMask());
        return document;
    }
    /**
     * Create an simple document from image.
     * @param image that will be used in document.
     * @param conf document config that will apply to document
     * @return an object DefaultDocument type
     * @throws IOException
     */
    public static DefaultDocument createDocument(BufferedImage image, DocumentConfiguration conf) throws IOException {

        /**
         * Caching data for better performance
         */
        if (fieldFactory == null) {
            fieldFactory = new FieldFactory(conf);
        }

        DefaultDocument document = new DefaultDocument(image);

        //image has no dpi
        Field[] fields = fieldFactory.fromConfig(0);
        document.setFieldList(fields);
        document.setMask(conf.getFileMask());
        return document;
    }



}
