package ru.gubkin.cb.document;

import ru.gubkin.cb.document.conf.DocumentConfiguration;
import ru.gubkin.cb.document.field.Field;

import java.awt.*;
import java.lang.ref.SoftReference;
import java.util.Set;

/**
 * Created by 119 on 18.01.2016.
 * Used for creating a field set from configuration.
 */
class FieldFactory {
    private FieldDecorator decorator = new FieldDecorator();
    private DocumentConfiguration configuration;

    FieldFactory (DocumentConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Create new field set from given configuration.
     * @param dpi fields for
     * @return a new field set
     *
     * TODO CACHE FIELD BY DEEP ARRAY COPY
     */
    public Field[] fromConfig(int dpi) {

        Set<String> keys = configuration.optionKeys(dpi);
        Field[] fields = new Field[keys.size()];
        int i = 0;
        for (String k : keys) {
            fields[i++] = createField(k, dpi);
        }

        return fields;
    }

    /**
     * Construct new field with name for dpi
     * @param name field name
     * @param dpi used for lookup filed location
     * @return a new full-decorated field
     */
    private Field createField(String name, int dpi) {
        Rectangle area = configuration.getFieldArea(name, dpi);
        Field f = new Field(name, area);
        decorator.decorate(f);

        return f;
    }

    class FieldDecorator {
        private static final String TESS_CONFIG_KEY = "tessConfig";
        private static final String CLEENUP_KEY = "cleenup";
        private static final String FIELD_LENGTH_KEY = "fieldLength";

        /**
         * Decoration a field with some thing from config.
         * @param f field for decor
         */
        public void decorate(Field f) {
            String tessConfig = configuration.getFieldOption(f.getName(), TESS_CONFIG_KEY);
            String cleenup = configuration.getFieldOption(f.getName(), CLEENUP_KEY);
            String fieldLength = configuration.getFieldOption(f.getName(), FIELD_LENGTH_KEY);

            if (!tessConfig.isEmpty()) {
                f.setTesseractCharsetDetail(tessConfig);
            }

            if (!cleenup.isEmpty()) {
                //default regexp cleenup method
                f.setCleaner((x) -> x.replaceAll(cleenup, ""));
            }

            if (!fieldLength.isEmpty()) {
                //default string validation method by length
                f.setChecker((x) -> x.length() == Integer.parseInt(fieldLength));
            }
        }
    }

}