package ru.gubkin.cb.document.field;

import java.awt.*;

/**
 * Created by 119 on 14.01.2016.
 * Class is represent document field.
 * Field has a name, value and area - absolute location in document in px.
 * Field can self-invalidate and clean value using cleaner.
 *
 */
public class Field implements Checkable, Cleanable {
    private String name;
    private String value;
    /**
     * Value that has been cleaned.
     */
    private String clearValue;
    /**
     * If field used specific characters, it may be specified
     * in tesseract config file.
     */
    private String tesseractCharsetDetail;

    private Checker checker;
    private Cleaner cleaner;

    private Rectangle area;

    /**
     * By default, field has a name and area
     * @param name field ("manid" for example)
     * @param area location in document
     */
    public Field(String name, Rectangle area) {
        this.name = name;
        this.area = area;
    }

    public void setChecker(Checker checker) {
        this.checker = checker;
    }

    public void setCleaner(Cleaner cleaner) {
        this.cleaner = cleaner;
    }

    @Override
    public boolean check() {
        //if checker is null - field is always invalid
        return checker != null && checker.check(clearValue);
    }

    @Override
    public boolean check(Checker c) {
        return c.check(clearValue);
    }

    @Override
    public void clean() {
        // no need clean if there is no cleaner
        if (cleaner != null) clean(cleaner);
    }

    @Override
    public void clean(Cleaner c) {
        clearValue = c.execute(value);
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getClearValue() {
        return clearValue;
    }

    public void setTesseractCharsetDetail(String tesseractCharsetDetail) {
        this.tesseractCharsetDetail = tesseractCharsetDetail;
    }

    public String getTesseractCharsetDetail() {
        return tesseractCharsetDetail;
    }

    public String getName() {
        return name;
    }

    public Rectangle getArea() {
        return area;
    }
}
