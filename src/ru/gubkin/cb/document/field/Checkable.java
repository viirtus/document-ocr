package ru.gubkin.cb.document.field;

/**
 * Created by 119 on 14.01.2016.
 * An interface mean, that class can be validate by some Checker.
 */
public interface Checkable {
    /**
     * Empty checker method.
     * @return is object valid?
     */
    boolean check();

    /**
     * Validate object using given checker.
     * @param c checker for validation.
     * @return is object valid?
     */
    boolean check(Checker c);
}
