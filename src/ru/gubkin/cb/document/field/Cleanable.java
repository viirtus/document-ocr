package ru.gubkin.cb.document.field;


/**
 * Created by 119 on 14.01.2016.
 * Class with it interface can be cleaned with given Cleaner or without.
 */
public interface Cleanable {
    /**
     * Clean class using internal methods.
     */
    void clean();

    /**
     * Clean class using specified Cleaner.
     * @param c used for cleenup
     */
    void clean(Cleaner c);
}
