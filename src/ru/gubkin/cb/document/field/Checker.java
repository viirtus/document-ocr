package ru.gubkin.cb.document.field;

/**
 * Created by 119 on 14.01.2016.
 * Interface show, that class is also checker.
 * In general, it is a functional interface.
 */
public interface Checker {
    /**
     * Method for checking given string for valid.
     * @param item for validation
     * @return is string valid?
     */
    boolean check(String item);
}
