package ru.gubkin.cb.document.field;

/**
 * Created by 119 on 14.01.2016.
 * Interface show, that class is also cleaner.
 * In general, it is a functional interface.
 */
public interface Cleaner {
    /**
     * Clean a given string and get a clear value.
     * @param str to clean
     * @return clear string
     */
    String execute(String str);
}
