package ru.gubkin.cb.document.conf;

import java.awt.*;
import java.io.IOException;
import java.util.Set;

/**
 * Created by 119 on 14.01.2016.
 * Class used for represent access interface to document configuration.
 *
 */
public class DocumentConfiguration {

    private final JsonConfReader reader;
    private String fileMask;

    /**
     * @param file conf file location. Must be a valid JSON.
     * @throws IOException if file is not accessible.
     */
    public DocumentConfiguration(String file) throws IOException {
        reader = new JsonConfReader(file);
        this.fileMask = reader.fileMask();
    }
    /** ---------------------------------8<(CUT HERE)----------------------------------------*/

    public Rectangle getFieldArea(String key, int dpi) {
        return reader.getFieldRect(key, dpi);
    }

    public String getFieldOption(String field, String option) {
        return reader.getFieldOption(field, option);
    }

    public String getMode() {
        return reader.getOption(JsonConfReader.MODE_KEY);
    }

    public String getSuccessFolder() {
        return reader.getOption(JsonConfReader.SUCCESS_FOLDER_KEY);
    }

    public String getFailFolder() {
        return reader.getOption(JsonConfReader.FAIL_FOLDER_KEY);
    }

    public String getSaveMode() {
        return reader.getOption(JsonConfReader.SAVE_AS_KEY);
    }

    public boolean isTransformAvailable() {
        return reader.keyExist(JsonConfReader.TRANSFORM_KEY);
    }


    public Object getTransform(String key) {
        return reader.getValue(JsonConfReader.TRANSFORM_KEY, key);
    }

    public Set<String> optionKeys(int dpi) {
        return reader.areaOptionKeys(dpi);
    }


    public String getFileMask() {
        return fileMask;
    }
}
