package ru.gubkin.cb.document.conf;

import org.json.JSONException;
import org.json.JSONObject;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Set;

/**
 * Created by 119 on 14.01.2016.
 * Simple key-value json reader with some work interface.
 */
public class JsonConfReader {
    private JSONObject root;
    protected static final String AREA_KEY = "area";
    protected static final String MODE_KEY = "mode";
    protected static final String SUCCESS_FOLDER_KEY = "successFolder";
    protected static final String FAIL_FOLDER_KEY = "failFolder";
    protected static final String FIELD_DEF_KEY = "fieldDef";
    protected static final String MASK_KEY = "mask";
    protected static final String SAVE_AS_KEY = "saveAs";
    protected static final String TRANSFORM_KEY = "transform";

    /**
     * Getting configuration root
     * @param file conf source
     * @throws IOException
     */
    JsonConfReader(String file) throws IOException {
        String rawJson = new String(Files.readAllBytes(new File(file).toPath()));
        root = new JSONObject(rawJson);
    }

    public String getOption(String option) {
        try {
            return root.getString(option);
        } catch (JSONException e) {
            return "";
        }
    }

    public String getFieldOption(String field, String option) {
        try {
            return root.getJSONObject(FIELD_DEF_KEY).getJSONObject(field).getString(option);
        } catch (JSONException e) {
            return "";
        }
    }


    public String fileMask() {
        String str = "";
        try {
            str = root.getString(MASK_KEY);
        } catch (JSONException e) {
            /** It is bad, but don't give a f**/
        }
        return str;
    }

    public Set<String> areaOptionKeys(int dpi) {
        JSONObject tmp = root.getJSONObject(AREA_KEY).getJSONObject(String.valueOf(dpi));
        return tmp.keySet();
    }

    public Rectangle getFieldRect(String key, int dpi) {
        Rectangle rect = new Rectangle();
        try {
            JSONObject tmp = root.getJSONObject(AREA_KEY).getJSONObject(String.valueOf(dpi)).getJSONObject(key);
            rect.x = tmp.getInt("x");
            rect.y = tmp.getInt("y");
            rect.width = tmp.getInt("width");
            rect.height = tmp.getInt("height");
        } catch (JSONException e) {
            /** It  is bad, but don't give a f**/
        }
        return rect;
    }

    public Object getValue(String key1, String key2) {
        try {
            return root.getJSONObject(key1).get(key2);
        } catch (JSONException e) {
            return null;
        }
    }

    public boolean keyExist(String key) {
        return root.has(key);
    }

}
