package ru.gubkin.cb.document;

import com.sun.istack.internal.NotNull;
import ru.gubkin.cb.document.field.Field;
import ru.gubkin.cb.util.Logger;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by 119 on 14.01.2016.
 * A default document. It extend simple document.
 * In general, used for all type of input document.
 */
public class DefaultDocument extends Document {
    /**
     * Document size in mm unit.
     */
    private static final float A4_X_WIDTH_MM = 215.39198f;
    private static final float A4_X_HEIGHT_MM = 298.28067f;
    /**
     * Document size in inch unit.
     */
    private static final float A4_X_WIDTH_I = A4_X_WIDTH_MM / 25.4f;
    private static final float A4_X_HEIGHT_I = A4_X_HEIGHT_MM / 25.4f;

    private static final String EMPTY_LOG_SPACE = "                      ";

    /**
     * Document dpi, if has.
     */
    private int dpi;

    /**
     * Document fields. By default, has no.
     */
    private Field[] fields = new Field[0];

    /**
     * Mask for OCR process. Output will be match mask.
     */
    private String mask;

    public DefaultDocument(File src) throws IOException {
        super(src);
        defineDpi();
    }

    public DefaultDocument(String src) throws IOException {
        super(src);
        defineDpi();
    }

    public DefaultDocument(@NotNull BufferedImage image) {
        super(image);
        defineDpi();
    }

    public void setFieldList(Field[] fields) {
        this.fields = fields;
    }


    public Field[] getFields() {
        return fields;
    }

    /**
     * Get a field by name
     * @param name field
     * @return a field, if has, null if no field.
     */
    public Field getField(String name) {
        for (Field f : fields) {
            if (f.getName().equals(name)) return f;
        }
        return null;
    }
    
    public void log(String logPath, boolean state) {
        StringBuilder builder = new StringBuilder();
        builder.append("Common state: ").append(Boolean.valueOf(state)).append(System.lineSeparator());
        //log each field state
        for (Field f : fields) {
            builder.append(EMPTY_LOG_SPACE).append("Field: ").append(f.getName()).append(System.lineSeparator());
            builder.append(EMPTY_LOG_SPACE).append("+----State: ").append(Boolean.valueOf(f.check())).append(System.lineSeparator());
            builder.append(EMPTY_LOG_SPACE).append("+----Before cleanup: ").append(f.getValue().replaceAll("[\n\r]", "<br>")).append(System.lineSeparator());
            builder.append(EMPTY_LOG_SPACE).append("+----After cleanup: ").append(f.getClearValue()).append(System.lineSeparator());
            Rectangle r = f.getArea();
            BufferedImage crop = image().getSubimage(r.x, r.y, r.width, r.height);
            //and field area image
            Logger.log(logPath, f.getName() + ".png", crop);
        }

        Logger.log(logPath, "log.txt", builder.toString());
        Logger.log(logPath, "raw.png", raw());
        Logger.log(logPath, "processed.png", image());
    }

    private void defineDpi() {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();

        // bad doc orientation
        if (sourceHeight < sourceWidth) {
            int tmp = sourceHeight;
            sourceHeight = sourceWidth;
            sourceWidth = tmp;
        }

        int xDpi = (int) (sourceWidth / A4_X_WIDTH_I);
        int yDpi = (int) (sourceHeight / A4_X_HEIGHT_I);
        if (xDpi > 250 && xDpi < 350) dpi = 300;

        if (xDpi > 350 && xDpi < 450) dpi = 400;

    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public String getMask() {
        return mask;
    }

    public int getDpi() {
        return dpi;
    }
}
